package com.example.dell.tempconverter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends Activity {
    private EditText text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.inputTemp);

    }

    public void myClickHandler(View view) {
        switch (view.getId()) {
            case R.id.btnConvert:
                RadioButton celsiusButton = (RadioButton) findViewById(R.id.rC);
                RadioButton fahrenheitButton = (RadioButton) findViewById(R.id.rF);

                float inputTemperature = Float.parseFloat(text.getText().toString());
                if (celsiusButton.isChecked()) {
                    text.setText(String
                            .valueOf(convertFahrenheitToCelsius(inputTemperature)));
                    celsiusButton.setChecked(false);
                    fahrenheitButton.setChecked(true);
                } else if (fahrenheitButton.isChecked()){
                    text.setText(String
                            .valueOf(convertCelsiusToFahrenheit(inputTemperature)));
                    fahrenheitButton.setChecked(false);
                    celsiusButton.setChecked(true);
                }
                break;
        }
    }


    private float convertFahrenheitToCelsius(float fahrenheit) {
        return ((fahrenheit - 32) * 5 / 9);
    }

    private float convertCelsiusToFahrenheit(float celsius) {
        return ((celsius * 9) / 5) + 32;
    }
}